Hello, this is a relatively simple AI program for playing Tic Tac Toe.

The concept behind the AI is as follows:
- Tic tac toe has a total of 19683 (3^9) board states.
- Each move in a game of tic tac toe creates a new board state.
- Each board state has an associated chance of creating a winning board state for the player.
- If an AI can simulate games enough times, it can create an educated decision for what move
  to make that will lead to the best chance of it winning.

The program is set at default to allow the AI to be imperfect to keep things entertaining.

A IDE capable of running C++ programs is required to run this program.

To run, open the file TicTacToe.cpp in a compatible IDE, then compile and run. 

If you have any questions, you are welcome to contact me at Jeremy.Suon@gmail.com

Enjoy. 