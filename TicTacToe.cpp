#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <string>
#include <locale>
#include <math.h>
#include <cmath>
#include <algorithm>
#include <limits>

using namespace std;

/* class objective: Entries in this hash table will hold 
 * the board state(as key) and win-loss counts(value)
 */
class HashTableEntry {
   public:
      int key;
      int value[2];
      HashTableEntry(int key, int value[2]) {
         this->key = key;
         this->value[0] = value[0];
		 this->value[1] = value[1];
      }
};

/* class objective: the hash table will be used to store all different
 * board states(entries) and their win-loss counts
 */
class HashMapTable{
	private:
		HashTableEntry **entries;
		int tableSize;
	public:
		/* initialize hash table */
		HashMapTable(int size){
			tableSize = size;
			entries = new HashTableEntry * [tableSize];
			for(int i = 0; i < tableSize; i++){
				entries[i] = NULL;
			}
		}
		
		/* retrieves index value of hash map */
		int HashFunc(int key){
			return key % tableSize;
		}
		
		/* inserts key-value pair into hash table */
		void Insert(int key, int value[2]){
			int index = HashFunc(key);
			while(entries[index] != NULL && entries[index]->key != key){
				index = HashFunc(index + 1);
			}
			if (entries[index] != NULL){
				delete entries[index];
			}
			entries[index] = new HashTableEntry(key, value);
		}
		
		/* returns the values of the hashtable entry given the key,
		 * provided that the entry is not null, otherwises returning [-1,-1] */
		int* SearchKey(int key, int values[2]){
			int index = HashFunc(key);
			values[0] = -1;
			values[1] = -1;
			while(entries[index] != NULL && entries[index]->key != key){
				index = HashFunc(index + 1);
			}
			if(entries[index] == NULL){
				return values;
			}
			else{
				values[0] = entries[index]->value[0];
				values[1] = entries[index]->value[1];
				return values;
			}
		}
		
		/* please note that the Remove function and deconstructor is not 
		 * defined in this implementation of a hash table as they were not 
		 * needed for the functionality of the program. */
};

/* function objective: return true if player provides correct 
 * coinflip guess, else return false.
 */
int guessFlip(int coinFlip){
	/* instantiate variables */
	int respFlag = 0;
	string playerGuess;
	locale loc;
	
	/* prompt player*/
	cout << "Heads or Tails? (Determines who goes first)" << endl;
	
	while(respFlag == 0){
		cin >> playerGuess;
		/* standardizes player response by making it all uppercase */
		for(string::size_type i = 0; i < playerGuess.length(); ++i){
			playerGuess[i] = toupper(playerGuess[i], loc);
		}
		
		/* checking if player guess is correct */
		if(playerGuess.compare("HEADS") == 0){
			respFlag = 1;
			if(coinFlip == 1){
				cout << "Correct! You go first." << endl << endl;
				return 1;
			}
			else{
				cout << "Incorrect. NPC will go first." << endl << endl;
				return 0;
			}
		}
		else if(playerGuess.compare("TAILS") == 0){
			respFlag = 1;
			if(coinFlip == 0){
				cout << "Correct! You will go first." << endl << endl;
				return 1;
			}
			else{
				cout << "Incorrect. NPC will go first." << endl << endl;
				return 0;
			}
		}
		else{
			cout << "Please input either HEADS or TAILS." << endl; 
		}
		
	}
	
	return 0;
}

/* function objective: display array of integers as a 
 * 3x3 grid.
 */
void displayBoard(int board[9]){
	/* instantiate variables */
	int index;
	
	for(int i = 0; i < 3; i++){
		for(int j = 0; j < 3; j++){
			index = i*3 + j;
			if(board[index] == -1){
				cout << "[ ]  ";
			}
			else if (board[index] == 1){
				cout << "[x]  ";
			}
			else{
				cout << "[o]  ";
			}
		}
		cout << endl;
	}
	cout << endl;
}

/* function objective: prompt the player for a valid tictactoe
 * coordinate and return the single integer equivalent.
 */ 
int getPlayerMove(int board[9]){
	/* instantiate variables */
	string playerMove;
	int respFlag = 0;
	int x, y, moveValue;
	
	while(respFlag == 0){
		/* prompt player */
		cout << "The board is indexed through x,y coordinates ranging from 1-3." << endl;
		cout << "Please input your move in coordinate form. i.e. (2,3)" << endl;
	
		cin >> playerMove;
		try{
			x = (playerMove[1] - '0') - 1;
			y = (playerMove[3] - '0') - 1;
			moveValue = (x*3) + y;
			if((moveValue <= 8) && (board[moveValue] == -1)){
				cout << endl;
				respFlag = 1;
				return moveValue;
			}
			else{
				cout << "Invalid move. Please enter another coordinate." << endl;
			}
		}
		catch(int e){
			cout << "Could not read coordinate. Please reinput your move." << endl;
		}
	}
	return 0;
}

/* function objective: determine an educated move for cpu based on
 * heuristics provided by hash map table. Convert from tictactoe 
 * coordinates to single integer and return.
 */
int getAIMove(int board[9], HashMapTable hash, int display = 1){
	int x = 0, y = 0, move = -1, keyValue = 0, useIfNoMove = 0, noMoveX = 0, noMoveY = 0;
	int values[2], coinFlip;
	float highestWinRatio = 0.0, tempRatio = -1.0;
	for(int i = 0; i < 9; i++){
		/* processes each valid move on the current board */
		if(board[i] == -1){
			
			/* calculates keyValue to look up in hash table */
			for(int j = 0; j < 9; j++){
				if((board[j] == 0) || (j == i)){
					keyValue += pow(3, j);
				}
				else if(board[j] == 1){
					keyValue += pow(3, j) * 2;
				}
			}
			values[0] = hash.SearchKey(keyValue, values)[0];
			values[1] = hash.SearchKey(keyValue, values)[1];
					
			/* AI will prioritize unexplored options during training */
			if(values[0] == -1 && display == 0){
				move = i;
				return move;
			}
			if((values[0] > 0) && (values[1] == 0)){
				tempRatio = 1;
			}
			else{
				if((values[0] > 1) && (values[1] == 0)){
					tempRatio = numeric_limits<float>::infinity();
				}
				else{
					tempRatio = (float)values[0] / (float)values[1];
				}
			}
			if(display == 1){
				cout << i << "->" << keyValue << "->" << values[0] << "," << values[1] << "->" << tempRatio << "->" << highestWinRatio << endl;
			}	
			/* selects the move with the highest chance of success */
			if(tempRatio > highestWinRatio){
				highestWinRatio = tempRatio;
				move = i;
				x = move / 3;
				y = move % 3;
			}
			else if(tempRatio == highestWinRatio){
				coinFlip = rand() % 2;
				if(coinFlip == 1){
					move = i;
					x = move / 3;
					y = move % 3;
				}
			}
		}
		keyValue = 0;
	}
	/* during training, if no moves are unexplored, a random move will be selected */
	if((display == 0) || (move == -1)){
		useIfNoMove = rand() % 9;
		while(board[useIfNoMove] != -1){
			useIfNoMove = rand() % 9;
		}
		noMoveX = useIfNoMove / 3;
		noMoveY = useIfNoMove % 3;
		if(display == 1){
			cout << "CPU moved to [" << noMoveX+1 << "," << noMoveY+1 << "]" << endl << endl;
			return useIfNoMove;
		}
		if(highestWinRatio < 9){
			return useIfNoMove;
		}
		
	}
	if(display == 1){
		cout << "CPU moved to [" << x+1 << "," << y+1 << "]" << endl << endl;
	}
	return move;
}

/* function objective: return true if tic tac toe 
 * is achieved in board. Else return false.
 */
int checkWin(int board[9]){
	/* there are 8 ways to achieve tictactoe. */
	for(int i = 0; i < 3; i++){
		/* vertical tictactoe check */
		if((board[i] != -1) && (board[i] == board[i+3]) && (board[i+3] == board[i+6])){
			return 1;
		}
		/* horizontal tictactoe check */
		if((board[i*3] != -1) && (board[i*3] == board[(i*3)+1]) && (board[(i*3)+1] == board[(i*3)+2])){
			return 1;
		}
	}
	/* diagonal tictactoe checks*/
	if((board[0] != -1) && (board[0] == board[4]) && (board[4] == board[8])){
		return 1;
	}
	if((board[2] != -1) && (board[2] == board[4]) && (board[4] == board[6])){
		return 1;
	}
	
	return 0;
}

/* function objective: return true if a tie in tic tac toe 
 * is achieved in board. Else return false.
 */
int checkTie(int board[9]){
	for(int i = 0; i < 9; i++){
		/* identifies an empty spot in the board */
		if(board[i] == -1){
			return 0;
		}
	}
	return 1;
}

/* function objective: return true if a game can be found with a number of no-null values 
 * amongst its options in the hashtable for each move in the game. False otherwise.
 */
int isTrainingDone(HashMapTable hash, int boardHistory[10][9], int turnNumber){
	int keyValue = 0, values[2], learnedOptionFlag = 0;
	float convergence = 0.0;
	for(int i = 0; i < turnNumber-1; i++){
		for(int j = 0; j < 9; j++){
			if(boardHistory[i][j] == -1){
				for(int k = 0; k < 9; k++){
					if((boardHistory[i][k] == 0) || (k == j)){
						keyValue += pow(3, j);
					}
					else if(boardHistory[i][k] == 1){
						keyValue += pow(3, j) * 2;
					}
				}
				values[0] = hash.SearchKey(keyValue, values)[0];
				values[1] = hash.SearchKey(keyValue, values)[1];
				if(values[0] != -1){
					learnedOptionFlag++;
				}
				keyValue = 0;
			}
		}
		if(learnedOptionFlag < 2){
			return 0;
		}
		learnedOptionFlag = 0;
	}
	return 1;
}

/* function objective: insert/update board states obtained from cpu game 
 * recorded in 2D array, boardHistory, into hash map table.
 */
HashMapTable logIntoHash(HashMapTable hash, int boardHistory[10][9], int turnNumber, int winner){
	int firstPlayerValue = 0;
	int secondPlayerValue = 0;
	int values[2];
	for(int i = 0; i < turnNumber; i++){
		for(int j = 0; j < 9; j++){
			/* "winning side" */
			if(boardHistory[i][j] == 0){
				firstPlayerValue += pow(3, j);
				secondPlayerValue += pow(3, j) * 2;
				//cout << "0 j->" << j << "->"<< firstPlayerValue << ", " << secondPlayerValue << endl;
			}
			/* "losing side" */
			if(boardHistory[i][j] == 1){
				firstPlayerValue += pow(3, j) * 2;
				secondPlayerValue += pow(3, j);
				//cout << "1 j->" << j << "->" << firstPlayerValue << ", " << secondPlayerValue << endl;
			}
		}
		/* record board history from 1st player perspective */
		values[0] = hash.SearchKey(firstPlayerValue, values)[0];
		values[1] = hash.SearchKey(firstPlayerValue, values)[1];
		if(values[0] == -1){
			if(winner == 0){
				values[0] = 1;
				values[1] = 1;
			}
			else if(winner == 1){
				values[0] = 1;
				values[1] = 0;
			}
			else{
				values[0] = 0;
				values[1] = 1;
			}
			hash.Insert(firstPlayerValue, values);
		}
		else{
			if(winner == 0){
				values[0] += 1;
				values[1] += 1;
			}
			else if(winner == 1){
				values[0] += 1;
				values[1] += 0;
			}
			else{
				values[0] += 0;
				values[1] += 1;
			}
			hash.Insert(firstPlayerValue, values);
		}
		
		/* record board history from 2nd player perspective */
		values[0] = hash.SearchKey(secondPlayerValue, values)[0];
		values[1] = hash.SearchKey(secondPlayerValue, values)[1];
		if(values[0] == -1){
			if(winner == 0){
				values[0] = 1;
				values[1] = 1;
			}
			else if(winner == 1){
				values[0] = 0;
				values[1] = 1;
			}
			else{
				values[0] = 1;
				values[1] = 0;
			}
			hash.Insert(secondPlayerValue, values);
		}
		else{
			if(winner == 0){
				values[0] += 1;
				values[1] += 1;
			}
			else if(winner == 1){
				values[0] += 0;
				values[1] += 1;
			}
			else{
				values[0] += 1;
				values[1] += 0;
			}
			hash.Insert(secondPlayerValue, values);
		}
		/* resets values */
		firstPlayerValue = 0;
		secondPlayerValue = 0;
	}
	return hash;
}

/* function objective: return a hashmaptable filled with the 
 * necessary heuristics for a CPU player to make intelligent 
 * actions when playing against the player.
 */
HashMapTable trainingCPU(HashMapTable hash){
	/* instantiate variables */
	int board[9] = {-1,-1,-1,-1,-1,-1,-1,-1,-1};
	int boardFlip[9] = {-1,-1,-1,-1,-1,-1,-1,-1,-1};
	int boardHistory[10][9];
	int playFlag = 0, winnerFlag = 0, numberOfGames = 0, turnNumber = 0;
	int cpuMove, otherCpuMove;
	for(int i = 0; i < 9; i++){
		boardHistory[0][i] = board[i];
	}
	
	/* CPU gameplay loop */
	while(playFlag == 0){
		while(winnerFlag == 0){
			cpuMove = getAIMove(board, hash, 0);
			board[cpuMove] = 0;
			turnNumber++;
			for(int i = 0; i < 9; i++){
				boardHistory[turnNumber][i] = board[i];
				if(board[i] == 0){
					boardFlip[i] = 1;
				}
				if(board[i] == 1){
					boardFlip[i] = 0;
				}
				if(board[i] == -1){
					boardFlip[i] = board[i];
				}
			}
			if(checkWin(board)){
				hash = logIntoHash(hash, boardHistory, turnNumber, 1);
				winnerFlag = 1;
			}
			else if(checkTie(board)){
				hash = logIntoHash(hash, boardHistory, turnNumber, 0);
				winnerFlag = 1;
			}
			else{
				otherCpuMove = getAIMove(boardFlip, hash, 0);
				board[otherCpuMove] = 1;
				turnNumber++;
				for(int i = 0; i < 9; i++){
					boardHistory[turnNumber][i] = board[i];
				}
				if(checkWin(board)){
					hash = logIntoHash(hash, boardHistory, turnNumber, 2);
					winnerFlag = 1;
				}
				else if(checkTie(board)){
					hash = logIntoHash(hash, boardHistory, turnNumber, 0);
					winnerFlag = 1;
				}
			}
		}

		/* checks if sufficient training has been filled */
		numberOfGames++;
		if(numberOfGames % 1000000 == 0){
			if(isTrainingDone(hash, boardHistory, turnNumber)){
				playFlag = 1;
			}
		}
		
		/* the greater this number, the minimal time training will take will increase, but the ai will perform more intelligently */
		if(numberOfGames > 20000000){
			playFlag = 1;
		}
		
		/* resets values */
		turnNumber = 0;
		winnerFlag = 0;
		for(int i = 0; i < 9; i++){
			board[i] = -1;
		}
	}
	
	return hash;
}

int main(){
	/* instantiate variables */
	int boardstates = pow(3,9);
	int coinFlip, playerGuess, playerMove, cpuMove;
	int board[9] = {-1,-1,-1,-1,-1,-1,-1,-1,-1};
	int playFlag = 0, winnerFlag = 0, againFlag = 0;
	HashMapTable hash(boardstates);
	string playAgain;
	locale loc;
	
	/* initialize random seed */
	srand (time(NULL));
	
	/* introductory statement */
	cout << "Welcome to Tic Tac Toe says Hello" << endl;
	cout << "We will be training a machine learning AI to play with you" << endl;
	cout << "Please wait one moment while we do so..." << endl;
	
	hash = trainingCPU(hash);
	
	cout << "Thank you for waiting! Let the game begin." << endl;
	
	while(playFlag == 0){
		/* resets values */
		againFlag = 0;
		for(int i = 0; i < 9; i++){
			board[i] = -1;
		}
		
		/* generate a hidden number between 0 and 1 */
		coinFlip = rand() % 2;
		
		/* Game Logic Sequence */
		if(guessFlip(coinFlip)){
			while(winnerFlag == 0){
				displayBoard(board);
				playerMove = getPlayerMove(board);
				board[playerMove] = 1;
				if(checkWin(board)){
					displayBoard(board);
					cout << "Congratulations, you win!" << endl;
					winnerFlag = 1;
				}
				else if(checkTie(board)){
					displayBoard(board);
					cout << "Tie!" << endl;
					winnerFlag = 1;
				}
				else{
					cpuMove = getAIMove(board, hash);
					board[cpuMove] = 0;
					if(checkWin(board)){
						displayBoard(board);
						cout << "CPU wins!" << endl;
						winnerFlag = 1;
					}
					else if(checkTie(board)){
						displayBoard(board);
						cout << "Tie!" << endl;
						winnerFlag = 1;
					}
				}
			}
		}
		else{
			while(winnerFlag == 0){
				cpuMove = getAIMove(board, hash);
				board[cpuMove] = 0;
				if(checkWin(board)){
					displayBoard(board);
					cout << "CPU wins!" << endl;
					winnerFlag = 1;
				}
				else if(checkTie(board)){
					displayBoard(board);
					cout << "Tie!" << endl;
					winnerFlag = 1;
				}
				else{
					displayBoard(board);
					playerMove = getPlayerMove(board);
					board[playerMove] = 1;
					if(checkWin(board)){
						displayBoard(board);
						cout << "Congratulations, you win!" << endl;
						winnerFlag = 1;
					}
					else if(checkTie(board)){
						displayBoard(board);
						cout << "Tie!" << endl;
						winnerFlag = 1;
					}
				}
			}
		}
		while(againFlag == 0){
			cout << "Would you like to play again? Yes or No" << endl;
			cin >> playAgain;
			
			/* standardizes player response by making it all uppercase */
			for(string::size_type i = 0; i < playAgain.length(); ++i){
				playAgain[i] = toupper(playAgain[i], loc);
			}
			
			/* sets/resets appropriate variables depending on player choice */
			if(playAgain.compare("YES") == 0){
				againFlag = 1;
				winnerFlag = 0;
				playFlag = 0;
			}
			else if(playAgain.compare("NO") == 0){
				againFlag = 1;
				playFlag = 1;
				cout << endl;
				cout << "Thank you for playing!" << endl;
			}
			else{
				cout << "Please input either Yes or No." << endl; 
			}
		}
	}
	return 0;
}